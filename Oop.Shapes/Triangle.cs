﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Oop.Shapes
{
    public class Triangle : Shape
    {
        #region сторона треугольника 1
        private int _side1;
        /// <summary>
        /// Сторона треугольника 1
        /// </summary>
        public int Side1
        {
            get => _side1;

            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("_side1", value, "Side1 is less than or equal to 0");
                }
                _side1 = value;
            }
        }
        #endregion

        #region сторона треугоника 2
        private int _side2;
        /// <summary>
        /// Сторона треугольника 2
        /// </summary>
        public int Side2
        {
            get => _side2;

            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("_side2", value, "Side2 is less than or equal to 0");
                }
                _side2 = value;
            }
        }
        #endregion

        #region сторона тругольника 3
        private int _side3;
        /// <summary>
        /// Сторона треугольника 3
        /// </summary>
        public int Side3
        {
            get => _side3;

            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("_side3", value, "Side3 is less than or equal to 0");
                }
                _side3 = value;
            }
        }
        #endregion

        public override double Area
        {
            get
            {
                double perimeter = Perimeter / 2f;

                return Math.Sqrt(perimeter * (perimeter - Side1) * (perimeter - Side2) * (perimeter - Side3));
            }
        }

        public override double Perimeter => Side1 + Side2 + Side3;

        public override int VertexCount => 3;

        public override bool IsEqual(Shape shape)
        {
            return shape is Triangle &&
                  (Side1 == ((Triangle)shape).Side1 && Side2 == ((Triangle)shape).Side2 && Side3 == ((Triangle)shape).Side3 ||
                   Side1 == ((Triangle)shape).Side1 && Side2 == ((Triangle)shape).Side3 && Side3 == ((Triangle)shape).Side2 ||
                   Side1 == ((Triangle)shape).Side2 && Side2 == ((Triangle)shape).Side1 && Side3 == ((Triangle)shape).Side3 ||
                   Side1 == ((Triangle)shape).Side2 && Side2 == ((Triangle)shape).Side3 && Side3 == ((Triangle)shape).Side1 ||
                   Side1 == ((Triangle)shape).Side3 && Side2 == ((Triangle)shape).Side1 && Side3 == ((Triangle)shape).Side2 ||
                   Side1 == ((Triangle)shape).Side3 && Side2 == ((Triangle)shape).Side2 && Side3 == ((Triangle)shape).Side1);
        }

        public Triangle(int side1, int side2, int side3)
        {
            Side1 = side1;
            Side2 = side2;
            Side3 = side3;

            if (side1 + side2 < side3 ||
                side1 + side3 < side2 ||
                side2 + side3 < side1)
            {
                throw new InvalidOperationException("Such triangle does not exist");
            }
        }
    }
}
