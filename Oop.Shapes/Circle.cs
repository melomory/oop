﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Oop.Shapes
{
    public class Circle : Shape
    {
        /// <summary>
        /// Радиус круга
        /// </summary>
        private int _radius;
        public int Radius
        {
            get => _radius;

            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("_radius", value, "Radius is less than or is equal to 0");
                }
                _radius = value;
            }
        }

        public override double Area => Math.PI * Radius * Radius;

        public override double Perimeter => 2 * Math.PI * Radius;

        public override int VertexCount => 0;

        public override bool IsEqual(Shape shape) => shape is Circle && Radius == ((Circle)shape).Radius;

        public Circle(int radius)
        {
            Radius = radius;    
        }
    }
}
