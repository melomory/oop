using NUnit.Framework;
using System;
using Oop.Shapes.Factories;

namespace Oop.Shapes.Tests
{
	[TestFixture]
	public class SquareTests
	{
		private ShapeFactory _shapeFactory;

		[OneTimeSetUp]
		public void FixtureSrtUp()
		{
			_shapeFactory = new ShapeFactory();
		}

		[TestCase(0)]
		[TestCase(-1)]
		[TestCase(int.MinValue)]
		public void Constructor_InvalidSide_ThrowsArgumentOutOfRangeException(int a)
		{
			Assert.Throws<ArgumentOutOfRangeException>(() =>
			{
				Shape _ = _shapeFactory.CreateSquare(a);
			});
		}

		[TestCase(1, ExpectedResult = 1)]
		[TestCase(5, ExpectedResult = 25)]
		[TestCase(12, ExpectedResult = 144)]
		public decimal Area___ReturnsExpectedValue(int a)
		{
			Shape square = _shapeFactory.CreateSquare(a);
			var area = square.Area;

			return (decimal)Math.Round(area, 0);
		}

		[TestCase(1, ExpectedResult = 4)]
		[TestCase(5, ExpectedResult = 20)]
		[TestCase(12, ExpectedResult = 48)]
		public decimal Perimeter___ReturnsExpectedValue(int a)
		{
			Shape square = _shapeFactory.CreateSquare(a);
			var perimeter = square.Perimeter;

			return (decimal)Math.Round(perimeter, 0);
		}

		[Test]
		public void VertexCount___Returns4()
		{
			Shape square = _shapeFactory.CreateSquare(1);
			var vertexCount = square.VertexCount;

			Assert.AreEqual(4, vertexCount);
		}

		[TestCase(4, ExpectedResult = true)]
		[TestCase(3, ExpectedResult = false)]
		[TestCase(5, ExpectedResult = false)]
		public bool IsEqual_OtherSquare_ReturnsExpectedResult(int a)
		{
			var square1 = _shapeFactory.CreateSquare(4);
			var square2 = _shapeFactory.CreateSquare(a);

			return square1.IsEqual(square2);
		}

		[Test]
		public void IsEqual_Circle_ReturnsFalse()
		{
			var square = _shapeFactory.CreateSquare(4);
			var circle = _shapeFactory.CreateCircle(4);

			var isEqual = square.IsEqual(circle);

			Assert.False(isEqual);
		}

		[Test]
		public void IsEqual_Triangle_ReturnsFalse()
		{
			var square = _shapeFactory.CreateSquare(4);
			var triangle = _shapeFactory.CreateTriangle(4, 5, 6);

			var isEqual = square.IsEqual(triangle);

			Assert.False(isEqual);
		}

		[Test]
		public void IsEqual_Rectangle_ReturnsFalse()
		{
			var square = _shapeFactory.CreateSquare(4);
			var rectangle = _shapeFactory.CreateRectangle(4, 5);

			var isEqual = square.IsEqual(rectangle);

			Assert.False(isEqual);
		}
	}
}