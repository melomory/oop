﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Oop.Shapes
{
    public class Square : Shape
    {
        /// <summary>
        /// Сторона квадрата
        /// </summary>
        private int _side;
        public int Side
        {
            get => _side;

            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("_side", value, "Side is less than or equal to 0");
                }
                _side = value;
            }
        }

        public override double Area => Side * Side;

        public override double Perimeter => 4 * Side;

        public override int VertexCount => 4;

        public override bool IsEqual(Shape shape) => shape is Square && Side == ((Square)shape).Side;

        public Square(int side)
        {
            Side = side;
        }
    }
}
