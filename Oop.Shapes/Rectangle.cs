﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Oop.Shapes
{
    public class Rectangle : Shape
    {
        #region ширина прямоугольника
        private int _width;
        /// <summary>
        /// Ширина прямоугольника
        /// </summary>
        public int Width {
            get => _width;

            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("_width", value, "Width is less than or equal to 0");
                }
                _width = value;
            }  
        }
        #endregion

        #region высота прямоугольника
        private int _height;
        /// <summary>
        /// Высота прямоугольника
        /// </summary>
        public int Height 
        {
            get => _height;

            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("_height", value, "Height is less than or equal to 0");
                }
                _height = value;
            }
        }
        #endregion

        public override double Area => Width * Height;

        public override double Perimeter => 2 * (Width + Height);

        public override int VertexCount => 4;

        public override bool IsEqual(Shape shape)
        {
            return shape is Rectangle && 
                  (Height == ((Rectangle)shape).Height && Width == ((Rectangle)shape).Width ||
                   Width == ((Rectangle)shape).Height && Height == ((Rectangle)shape).Width);
        }

        public Rectangle(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }
}
